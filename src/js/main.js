let burgerButton = document.querySelector('.header__menu');
console.log(burgerButton);
let dropMenu = document.querySelector('.header__menu-items');
console.log(dropMenu);

burgerButton.addEventListener('click', function(event){
    if (!dropMenu.classList.contains('active-tabs')) {
        dropMenu.classList.add('active-tabs');
        console.log(event);
        burgerButton.innerHTML = `<?xml version="1.0" ?><svg class="cross-icon" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><defs><style>.cls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px;}</style></defs><title/><g id="cross"><line class="cls-1" x1="7" x2="25" y1="7" y2="25"/><line class="cls-1" x1="7" x2="25" y1="25" y2="7"/></g></svg>`;
    }
    else {
        dropMenu.classList.remove('active-tabs');
        console.log(event);
        burgerButton.innerHTML = `<i class="fa-sharp fa-solid fa-bars fa-lg"></i>`;
    }
})