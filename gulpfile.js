const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browsersync = require('browser-sync').create();
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const purgecss = require('gulp-purgecss')

function browserServe(finish) {
    browsersync.init({
        server: {
            baseDir: './' 
        }
    })

    finish()
}

function browserReload(finish) {
    browsersync.reload();
    finish()
}

function cleandist(finish) {
    del.sync('dist')
    finish()
}

function css() {
    return gulp.src('./src/style/**/*.scss')

    .pipe(sass())

    .pipe(autoprefixer())

    .pipe(cleanCSS())

    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest('./dist'))
}

function javascript() {
    return gulp.src('./src/js/**/*.js')
 
    .pipe(concat('scripts.min.js'))

    .pipe(uglify())

    .pipe(gulp.dest('./dist'))
}


function optimizeImages() {
    return gulp.src('./src/img/**.*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'))
}

function watch() {
    gulp.watch('src/**/*.*', gulp.series(cleandist, gulp.parallel(css, javascript), browserReload))
}

exports.cleandist = cleandist;
exports.css = css;
exports.javascript = javascript;
exports.optimizeImages = optimizeImages;

exports.build = gulp.series(cleandist, gulp.parallel(css, javascript), optimizeImages);

exports.dev = gulp.series(gulp.parallel(css, javascript), browserServe, watch);